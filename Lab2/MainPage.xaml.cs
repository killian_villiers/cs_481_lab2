﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public IList<Course> Courses { get; private set; }
        public MainPage()
        {
            InitializeComponent();
            Courses = new List<Course>();
            Courses.Add(new Course
            {
                Name = "Baguette",
                Location = "SuperU",
                ImageUrl = "baguette.jpeg",
                ImageUrl2 = "1.jpeg"
            });
            Courses.Add(new Course
            {
                Name = "Cheese",
                Location = "Leclerc",
                ImageUrl = "cheese.jpeg",
                ImageUrl2 = "2.jpg"
            });
            BindingContext = this;

        }


    }
}
